" ~/.vimrc from Jose Luis Rivas <me@ghostbar.co>

source ~/.vim/rc/vundle.vimrc

source ~/.vim/rc/basics.vimrc

source ~/.vim/rc/copy-n-paste.vimrc

source ~/.vim/rc/search.vimrc

source ~/.vim/rc/tabs.vimrc

source ~/.vim/rc/filetype.vimrc

source ~/.vim/rc/colorscheme.vimrc

" Hacks
source ~/.vim/rc/silver-searcher.vimrc

"""""""""""""""""""""""""""""""""""
" Plugins start here
" 
"""""""""""""""""""""""""""""""""""

source ~/.vim/rc/nerdtree.vimrc

source ~/.vim/rc/ctrlp.vimrc

source ~/.vim/rc/indentline.vimrc

source ~/.vim/rc/powerline.vimrc
